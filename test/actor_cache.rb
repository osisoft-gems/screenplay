$:.unshift File.join(File.dirname(__FILE__), '..', 'lib')

require 'minitest/autorun'
require 'screenplay/actors/cache'

module Screenplay

	class CacheActorTest < MiniTest::Unit::TestCase

		def setup
			@actor = CacheActor.new('cache')
		end

		def test_cache_actor
			input1 = { id: 'husker', name: 'William Adama' }
			input2 = { names: ['William Adama', 'Lee Adama'] }

			@actor.play({ set: {callsign: :id}}, input1 )
			assert_equal({ callsign: 'husker' }, @actor.cache)

			cached = { an_id: 'husker' }
			assert_equal(cached, @actor.play({get: {an_id: :callsign}}, input1))
			assert_equal(input1.merge(cached), @actor.play({merge: {an_id: :callsign}}, input1))

			@actor.play({clear: nil}, {})
			assert_equal({}, @actor.cache)

			@actor.play({set: {cached_names: :names}}, input2)
			assert_equal({cached_names: ['William Adama', 'Lee Adama']}, @actor.cache)

			@actor.play({clear: nil}, {})
			@actor.play({set: {cached_names: '$input'.to_sym}}, input2)
			assert_equal({cached_names: {names: ['William Adama', 'Lee Adama']}}, @actor.cache)
		end

		def test_get_value_from_path(path, data)
			input = {
				callsign: 'husker',
				name: 'William Adama',
				wifes: ['Carolanne Adama'],
				children: ['Zak Adama', 'Lee Adama'],
				service_record: {
					'D6/21311'.to_sym => 'First commission: Battlestar Galactica air group',
					'E4/21312'.to_sym => 'Commendation for shooting down Cylon fighter in first viper combat mission',
					'D5/21314'.to_sym => 'Mustered out of service post-armistice',
					'R6/21317'.to_sym => 'Served as Deck Hand in merchant fleet and as common [...] aboard inter-colony tramp freighters',
					'D1/21331'.to_sym => 'Recommissioned in Colonial Fleet',
					'D6/21337'.to_sym => 'Major: Battlestar Atlantia',
					'R8/21341'.to_sym => 'Executive Officer: Battlestar Columbia',
					'C2/21345'.to_sym => 'Commander: Commanding Officer, Battlestar Valkyrie',
					'C2/21348'.to_sym => 'Commander: Commanding Officer, Battlestar Galactica'
				},
			  hash_test: {
				  test1: 'Hello world',
				  deeper: {
						test2: 'very deep'
					}
			  },
				array_test: [{
						test3: 'Better world',
						test4: ['Better', 'World']
				}]
			}

			assert_equal('husker', @actor.get_recursive_value('callsign', input))
			assert_equal('Major: Battlestar Atlantia', @actor.get_recursive_value('service_record.D6/21337', input))
		end

	end

end