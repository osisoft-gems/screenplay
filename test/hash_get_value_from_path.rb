$:.unshift File.join(File.dirname(__FILE__), '..', 'lib')

require 'minitest/autorun'
require 'screenplay/datatype-extensions'

class HashGetValueFromPathTester < MiniTest::Unit::TestCase

	def test_get_value_from_path
		input = {
			Character: {
				id: 'husker',
				name: 'William Adama',
				name_details: {
					given_name: 'William',
				  surname: 'Adama'
				},
				children: [ 'Lee', 'Zack' ]
			}
		}
		assert_equal('husker', input.get_value_from_path('Character.id'))
		assert_equal('William Adama', input.get_value_from_path('Character.name'))
		assert_equal('William', input.get_value_from_path('Character.name_details.given_name'))
		assert_equal('Zack', input.get_value_from_path('Character.children[1]'))

	end

end
